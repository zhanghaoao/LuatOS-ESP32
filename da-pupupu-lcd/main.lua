--- 模块功能：lcddemo 
-- @module lcd
-- @author Dozingfiretruck
-- @release 2021.01.25
-- LuaTools需要PROJECT和VERSION这两个信息
-- 1

PROJECT = "lcddemo"
VERSION = "1.0.0"

log.info("main", PROJECT, VERSION)

-- sys库是标配
_G.sys = require("sys")

-- 添加硬狗防止程序卡死
wdt.init(15000)--初始化watchdog设置为15s
sys.timerLoopStart(wdt.feed, 10000)--10s喂一次狗


spi_lcd = spi.deviceSetup(2, 7, 0, 0, 8, 40000000, spi.MSB, 1, 1)


--[[ 此为合宙售卖的1.8寸TFT LCD LCD 分辨率:128X160 屏幕ic:st7735 购买地址:https://item.taobao.com/item.htm?spm=a1z10.5-c.w4002-24045920841.19.6c2275a1Pa8F9o&id=560176729178]]
lcd.init("st7735",{port = "device",pin_dc = 6, pin_pwr = 11,pin_rst = 10,direction = 0,w = 128,h = 160,xoffset = 2,yoffset = 1},spi_lcd)


--如果显示颜色相反，关闭反色
lcd.invoff()

--如果显示依旧不正常，可以尝试老版本的板子的驱动
--lcd.init("st7735s",{port = "device",pin_dc = 6, pin_pwr = 11,pin_rst = 10,direction = 2,w = 160,h = 80, xoffset = 1,yoffset = 26},spi_lcd)



function shouping()
        
    -- 显示之前先设置为中文字体,对之后的drawStr有效,
    -- 使用中文字体需在luat_conf_bsp.h.h开启
    -- #define USE_U8G2_OPPOSANSMxx_CHINESE xx代表字号     12/16 已自己打包
    lcd.setFont(lcd.font_opposansm12)
    lcd.drawStr(40,10,"drawStr-time")
    sys.wait(2000)
    lcd.setFont(lcd.font_opposansm16_chinese)
    lcd.drawStr(40,40,"drawStr中文 测试")



    lcd.showImage(0,0,"/jpg/home1.jpg")
    sys.wait(100)



    -- 初始化lcd的buff缓冲区, 可理解为FrameBuffer区域.
    lcd.setupBuff()

    -- 设置buff 并禁用自动更新
    lcd.setupBuff()
    lcd.autoFlush(false)
    -- 禁止自动更新后, 需要使用 lcd.flush() 主动刷新数据到屏幕

    -- 主动刷新数据到界面, 仅设置buff且禁用自动属性后使用
    lcd.flush()

end














-- sys.taskInit(function() 
--     sys.wait(1500)


--     if lcd.showImage then

--         -- 图片写入缓冲区
--         --lcd.showImage(0,0,"/jpg/baiping.jpg")
--         lcd.showImage(0,0,"/jpg/home1.jpg")
--         sys.wait(100)
--         -- lcd.showImage(0,0,"/jpg/home2.jpg")
--         -- lcd.showImage(0,0,"/jpg/jishizhong.jpg")
--         -- lcd.showImage(0,0,"/jpg/kuaishujishi.jpg")
--         -- lcd.showImage(0,0,"/jpg/ok.jpg")
--         -- lcd.showImage(0,0,"/jpg/tixing.jpg")
--         -- lcd.showImage(0,0,"/jpg/todo.jpg")


--     else
--         log.info("lcd.drawLine", lcd.drawLine(20, 20, 150, 20, 0x001F))
--         log.info("lcd.drawRectangle", lcd.drawRectangle(20, 40, 120, 70, 0xF800))
--         log.info("lcd.drawCircle", lcd.drawCircle(50, 50, 20, 0x0CE0))
--     end

-- end)





-- 用户代码已结束---------------------------------------------
-- 结尾总是这一句
sys.run()
-- sys.run()之后后面不要加任何语句!!!!!
