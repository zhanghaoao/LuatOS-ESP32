-- 可以循环展示图片
--- 模块功能：lcddemo
-- @module lcd
-- @author Dozingfiretruck
-- @release 2021.01.25
-- LuaTools需要PROJECT和VERSION这两个信息
-- 用于

PROJECT = "lcddemo"
VERSION = "1.0.0"
log.info("main", PROJECT, VERSION)






-- sys库是标配
_G.sys = require("sys")
local sys = require "sys"




-- 添加硬狗防止程序卡死
wdt.init(15000)--初始化watchdog设置为15s
sys.timerLoopStart(wdt.feed, 10000)--10s喂一次狗








sys.taskInit(
    function()
        log.info("wlan", "wlan_init:", wlan.init())
        log.info("mode", wlan.setMode(wlan.STATION))
        log.info("connect", wlan.connect("88888888", "88888888"))
        -- 等待连上路由,此时还没获取到ip
        result, _ = sys.waitUntil("WLAN_STA_CONNECTED")
        log.info("wlan", "WLAN_STA_CONNECTED", result)
        -- 等到成功获取ip就代表连上局域网了
        result, data = sys.waitUntil("IP_READY")
        log.info("wlan-----------", "IP_READY", result, data)
        -- 北京时间,具体参数请看(https://www.gnu.org/software/libc/manual/html_node/TZ-Variable.html)
        ntp.settz("CST-8")
        ntp.init("ntp.ntsc.ac.cn")
    end
)

sys.subscribe(
    "NTP_SYNC_DONE",
    function()
        log.info("ntp", "done")
        log.info("date", os.date())
        timenow=os.date()
    end
)









spi_lcd = spi.deviceSetup(2, 7, 0, 0, 8, 40000000, spi.MSB, 1, 1)
--[[ 此为合宙售卖的1.8寸TFT LCD LCD 分辨率:128X160 屏幕ic:st7735 购买地址:https://item.taobao.com/item.htm?spm=a1z10.5-c.w4002-24045920841.19.6c2275a1Pa8F9o&id=560176729178]]
lcd.init("st7735",{port = "device",pin_dc = 6, pin_pwr = 11,pin_rst = 10,direction = 2,w = 128,h = 160,xoffset = 0,yoffset = 0},spi_lcd)--说明地址https://wiki.luatos.com/api/lcd.html
--如果显示颜色相反，关闭反色
lcd.invoff()
--如果显示依旧不正常，可以尝试老版本的板子的驱动
--lcd.init("st7735s",{port = "device",pin_dc = 6, pin_pwr = 11,pin_rst = 10,direction = 2,w = 160,h = 80, xoffset = 1,yoffset = 26},spi_lcd)











function shouping(jpg1)

    -- 显示之前先设置为中文字体,对之后的drawStr有效,
    -- 使用中文字体需在luat_conf_bsp.h.h开启
    -- #define USE_U8G2_OPPOSANSMxx_CHINESE xx代表字号     12/16 已自己打包
    lcd.showImage(0,0,jpg1)--传入图片
    lcd.setFont(lcd.font_opposansm12_chinese)

    -- 获取时间
    --timenow=os.date()

    -- 书写出时间
    --lcd.drawStr(5,110,timenow)

    -- GET请求
    http.req("https://v1.jinrishici.com/all.txt", nil,
        function(ret, code, headers, body)
            log.info("http", ret, code, header, body)
            lcd.setFont(lcd.font_opposansm12_chinese)
            lcd.drawStr(5,110,body)
        end)






    -- 主动刷新数据到界面, 仅设置buff且禁用自动属性后使用
    lcd.flush()

    -- sys.wait(1000)
    lcd.setFont(lcd.font_opposansm16_chinese)
    --lcd.drawStr(25,130,"25:31分")
    sys.wait(100)






    -- 初始化lcd的buff缓冲区, 可理解为FrameBuffer区域.
    lcd.setupBuff()

    -- 设置buff 并禁用自动更新
    lcd.setupBuff()
    lcd.autoFlush(false)
    -- 禁止自动更新后, 需要使用 lcd.flush() 主动刷新数据到屏幕

    -- 主动刷新数据到界面, 仅设置buff且禁用自动属性后使用
    lcd.flush()




end

-- sys.taskInit(function()
--     sys.wait(1500)

--     if lcd.showImage then

--         -- 图片写入缓冲区
--         --lcd.showImage(0,0,"/jpg/baiping.jpg")
--         lcd.showImage(0,0,"/jpg/home1.jpg")
--         sys.wait(100)
--         -- lcd.showImage(0,0,"/jpg/home2.jpg")
--         -- lcd.showImage(0,0,"/jpg/jishizhong.jpg")
--         -- lcd.showImage(0,0,"/jpg/kuaishujishi.jpg")
--         -- lcd.showImage(0,0,"/jpg/ok.jpg")
--         -- lcd.showImage(0,0,"/jpg/tixing.jpg")
--         -- lcd.showImage(0,0,"/jpg/todo.jpg")


--     else
--         log.info("lcd.drawLine", lcd.drawLine(20, 20, 150, 20, 0x001F))
--         log.info("lcd.drawRectangle", lcd.drawRectangle(20, 40, 120, 70, 0xF800))
--         log.info("lcd.drawCircle", lcd.drawCircle(50, 50, 20, 0x0CE0))
--     end

-- end)


--设置随机种子
math.randomseed(os.time())
ccc = math.random(1,32)

if ccc==1 then
    jpg1="/luadb/home1.jpg"

elseif ccc==2 then
    jpg1="/luadb/home2.jpg"
elseif ccc==3 then
    jpg1="/luadb/jishizhong.jpg"
elseif ccc==4 then
    jpg1="/luadb/kuaishujishi.jpg"
elseif ccc==5 then
    jpg1="/luadb/ok.jpg"
elseif ccc==6 then
    jpg1="/luadb/tixing.jpg"
elseif ccc==7 then
    jpg1="/luadb/todo.jpg"
else
    math.randomseed(os.time())
    bbb = math.random(1,27)
    jpg1= "/luadb/"..bbb..".jpg"
    log.info("图片地址", jpg1)
end

shouping(jpg1)












-- LuaTools需要PROJECT和VERSION这两个信息
PROJECT = "gpio2demo"
VERSION = "1.0.0"

log.info("main", PROJECT, VERSION)

-- sys库是标配
_G.sys = require("sys")

if wdt then
    --添加硬狗防止程序卡死，在支持的设备上启用这个功能
    wdt.init(15000)--初始化watchdog设置为15s
    sys.timerLoopStart(wdt.feed, 10000)--10s喂一次狗
end

local button_timer_outtime = 10 --按键定时器: 10ms
local button_shake_time = 1     --按键消抖时间: button_shake_time*button_timer_outtime
local button_long_time = 100    --按键长按时间: button_shake_time*button_timer_outtime

local button_detect = true
local button_state = false
local button_cont = 0

local BTN_PIN = 9 -- 我选boot
-- 按实际开发板选取

-- 若固件支持防抖, 启用防抖
if gpio.debounce then
    gpio.debounce(BTN_PIN, 5)
end

button = gpio.setup(BTN_PIN, function()
        if not button_detect then return end
        button_detect = false
        button_state = true
    end,
    gpio.PULLUP,
    gpio.FALLING)

button_timer = sys.timerLoopStart(function()
    if button_state then
        if button() == 0 then
            button_cont = button_cont + 1
            if button_cont > button_long_time then
                print("long pass")
            end
        else
            if button_cont < button_shake_time then
            else

                if button_cont < button_long_time then
                    print("pass")
                    -- GET请求
					http.req("http://v1.jinrishici.com/all.txt", nil, function(ret, code, headers, body)
                        log.info("http", ret, code, header, body)
                        lcd.setFont(lcd.font_opposansm12_chinese)
                        lcd.drawStr(5,110,body)
                        lcd.drawStr(5,121,"111111")
                        end)
                else
                    print("long pass")
                end
            end
            button_cont = 0
            button_state = false
            button_detect = true
        end
    end
end,button_timer_outtime)















-- 用户代码已结束---------------------------------------------
-- 结尾总是这一句
sys.run()
-- sys.run()之后后面不要加任何语句!!!!!
